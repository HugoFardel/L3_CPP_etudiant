#!/bin/sh

g++ -O2 -Wall -o main.out main.cpp

./main.out > out-tree.csv

IMG_EXT="png"

PLOT_PARAMS=" \
    set terminal ${IMG_EXT} size 640,480 ; \
    set key left top ; \
    set xtics rotate by -45 ; \
    set grid xtics ytics; \
    set rmargin at screen 0.9 ; " 

gnuplot -e "set out \"out-vector.${IMG_EXT}\" ; \
    ${PLOT_PARAMS} \
    plot 'out-tree.csv' using 1:2 with line lc rgb 'red' lw 2 title 'vector'"

# TODO plot tree + cmp

