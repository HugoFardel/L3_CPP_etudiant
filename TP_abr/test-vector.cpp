#include "vector.hpp"

int main() {
    std::vector<int> v;
    v.push_back(13);
    v.push_back(86);
    v.push_back(42);
    v.push_back(37);
    v.push_back(42);

    print(v);

    std::cout << "search(7): " << search(v, 7) << std::endl;
    std::cout << "search(37): " << search(v, 37) << std::endl;
    std::cout << "search(40): " << search(v, 40) << std::endl;
    std::cout << "search(42): " << search(v, 42) << std::endl;

    return 0;
}

